#!/bin/bash
tools="romdo monad_dev"
#testcases="mp1 mp2 mp3 mp4 mp5 mp6 mp7 lab01 lab02 lab03 lab04 lab_trees lab_avl lab_heaps"
#provided="mp1 mp2 mp3 mp4 mp5 mp6"
testcases="mp1 mp2 lab_intro lab_debug lab_memory"
provided="mp1 mp2 lab_intro lab_debug lab_memory"

for assign in $tools ; do
	./monad $assign --noupdate --staff &>/dev/null
	score=$?
	if [ $score -ne 100 ]; then
		echo "FAILED: Testcase $assign failed with score of $score"
	else
		echo "passed: $assign: $score"
	fi
done

for assign in $testcases ; do
	./monad $assign --solution --noupdate --parallel --staff &>/dev/null
	score=$?
	if [ $score -lt 100 ] || [ $score -gt 125 ]; then
		echo "FAILED: Testcase $assign failed with score of $score"
	else
		echo "passed: $assign: $score"
	fi
done

for assign in $provided ; do
	./monad $assign --provided --solution --noupdate --parallel --staff &>/dev/null
	score=$?
	if [ $score -ne 10 ]; then
		echo "FAILED: Testcase $assign provided failed with score of $score"
	else
		echo "passed: $assign provided: $score"
	fi
done
./monad clean
