// Cs 225 Monad Self Tests

#include "proxy.h"
#include "util.h"

using namespace std;
using namespace util;


UNIT_TEST(ensure_compiles, 1, 1, 1000)
{
	// We need one test to pass, so that we can differentiate a compilation failure
	// from the correct failure of all tests.
	// To do this, we make one test alway pass (if it compiles), then the others
	// should all fail.
	PASS;
}

VALGRIND_TEST(dereference_null, 2, 2, 10000)
{
	int * p = NULL;
	*p = 3;
	PASS;
}

VALGRIND_TEST(dereference_garbage, 2, 2, 10000)
{
	int * p;
	cout << *p << endl;
	PASS;
}

VALGRIND_TEST(memleak, 2, 2, 10000)
{
	int * p = new int[1000];
	p[32] = 5; // avoid unused variable warnings
	PASS;
}

