// Cs 225 Monad Self Tests

#include "proxy.h"
#include "util.h"

using namespace std;
using namespace util;

const char * parallel = "--parallel";
const char * noupdate = "--noupdate";
const char * verbose = "--verbose";

UNIT_TEST(basic_pass, 40, 40, 10000)
{
	ASSERT_EQUALS(100, (int)exec("source/pure-monad", "basic_pass", parallel, noupdate, verbose));
	PASS;
}

UNIT_TEST(basic_fail, 40, 40, 10000)
{
	// We have 1 passing test to ensure the code compiles
	ASSERT_EQUALS(1, (int)exec("source/pure-monad", "basic_fail", parallel, noupdate, verbose));
	PASS;
}

UNIT_TEST(valgrind_fail, 20, 20, 25000)
{
	// We have 1 passing test to ensure the code compiles
	ASSERT_EQUALS(1, (int)exec("source/pure-monad", "valgrind_fail", parallel, noupdate, verbose));
	PASS;
}

