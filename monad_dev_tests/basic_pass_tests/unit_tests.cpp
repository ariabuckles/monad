// Cs 220 Monad Self Tests

#include "proxy.h"
#include "util.h"

using namespace std;
using namespace util;

int sumDigits(int n)
{
    if (n == 0) return 0;
    return n % 10 + sumDigits(n / 10);
}

UNIT_TEST(basic_pass, 0, 1, 1000)
{
	PASS;
}

UNIT_TEST(basic_implicit_pass, 0, 1, 1000)
{
	// implicit pass
}

UNIT_TEST(sumDigits_762, 0, 1, 1000)
{
	ASSERT(sumDigits(762) == 15);
	PASS;
}

HELPER_TEST(myHelper, int x)
{
	ASSERT(x == 3);
	PASS;
}

UNIT_TEST(testHelper, 0, 1, 1000)
{
	CALL_HELPER(myHelper, 3);
	PASS;
}

void myHelper2(int x)
{
	ASSERT_EQUALS(4, x);
}

UNIT_TEST(testFreeformHelper, 0, 1, 1000)
{
	myHelper2(4);
}

UNIT_TEST(testAssertOutput, 0, 1, 1000)
{
	cout << "some sample output" << endl;
	ASSERT_OUTPUT(contains, "some sample output");
	PASS;
}

UNIT_TEST(testAssertOutput2, 0, 94, 1000)
{
	cout << "some sample2 output" << endl;
	ASSERT_OUTPUT(contains, "sample2");
	PASS;
}

