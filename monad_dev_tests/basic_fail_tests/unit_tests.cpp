// Cs 225 Monad Self Tests

#include "proxy.h"
#include "util.h"

using namespace std;
using namespace util;


UNIT_TEST(ensure_compiles, 1, 1, 1000)
{
	// We need one test to pass, so that we can differentiate a compilation failure
	// from the correct failure of all tests.
	// To do this, we make one test alway pass (if it compiles), then the others
	// should all fail.
	PASS;
}

UNIT_TEST(simple_fail, 2, 2, 1000)
{
	FAIL("testing FAIL()");
}

UNIT_TEST(simple_assert, 2, 2, 1000)
{
	ASSERT(0 == 1);
	PASS;
}

UNIT_TEST(dereference_null, 2, 2, 10000)
{
	int * p = NULL;
	*p = 3;
	PASS;
}

UNIT_TEST(timeout, 2, 2, 100)
{
	while (true)
	{ }
	PASS;
}

HELPER_TEST(myHelper, int x)
{
	ASSERT(x == 3);
	PASS;
}

UNIT_TEST(testHelper, 2, 2, 1000)
{
	CALL_HELPER(myHelper, 4);
	PASS;
}

void myHelper2(int x)
{
	ASSERT_EQUALS(3, x);
}

UNIT_TEST(testFreeformHelper, 2, 2, 1000)
{
	myHelper2(5);
}

UNIT_TEST(testAssertOutput, 2, 2, 1000)
{
	cout << "haha gotcha" << endl;
	ASSERT_OUTPUT(contains, "sample");
}

